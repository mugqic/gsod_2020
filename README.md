# Welcome to the C3G Google Season of Docs 2020: BENTO/CHORD project:


--------------
Table of content:

[TOC]

--------------


## About Season of Docs

Season of Docs is a new Google program, launched in 2019, to pair technical writers with open source organizations. The goal of the program is to provide a framework for technical writers and open source projects to work together towards the common goal of improving open source projects' documentation.

For more information about the program, please consult the [Season of Docs documentation](https://developers.google.com/season-of-docs/).


## About C3G

The Canadian Centre for Computational Genomics, C3G for short, is an academically-affiliated entity that provides bioinformatics analysis and HPC services for the life science research community. Some of our main objectives are:

+ Writing [Open Source software](http://www.computationalgenomics.ca/tools/) to facilitate bioinformatics analysis.
+ Providing [data analysis services](http://www.computationalgenomics.ca/services/) to the genomics community.
+ Training graduate students and researchers through bioinformatics [workshops](http://www.computationalgenomics.ca/2019-2/).
+ Providing support for large scale genomics projects like [IHEC](http://ihec-epigenomes.org/), [Profyle](https://www.terryfox.org/recent-posts/profyle/) & [BRIDGET](https://bridget.u-bordeaux.fr/).
+ Creating large scale databases and portals for large projects like [IHEC](https://epigenomesportal.ca/ihec/), in an attempt to organize public data.


For more information about C3G, please consult [our website](http://www.computationalgenomics.ca/).


## About BENTO/CHORD

CHORD, “Canadian Health ‘Omics Repository, Distributed” is a set of software services developed by C3G at McGill University. The aim of CHORD is to enable large-scale genomic analyses across private datasets controlled by the local institutions who are responsible for the data. 


It is a platform to share genomic and epigenomic (meta)data in a federated way. It can store:


* patient, sample, experiment and analysis metadata
* genomic data such as VCF and BAMs
* epigenomic data such as bigwigs


## Season of Docs Project Ideas

Currently, CHORD documentation is limited on [Read the Docs](https://chord-docs.readthedocs.io/en/latest/introduction/chord.html) and on our github [repo](https://github.com/c3g). We need help to consolidate all the documentation on a single resource and to simplify documentation to non-expert users, if needed. We are looking for a technical writer that is enthusiatics about open source, and has some interest in the life science research community. We look forward to working together to provide better, more comprehensive documentation for our users.

### Idea 1: Set up BENTO/CHORD docs on [Read the Docs](https://readthedocs.org/)

We currently have a very limited and crude version of the BENTO/CHORD documentation on [Read the Docs](https://chord-docs.readthedocs.io/en/latest/introduction/chord.html). We would like to expand it. We need a technical writer than can go through all the available, scattered docs and organize it elegantly to the public.



## Required Skills

+ Python
+ basic bioinformatics is an asset


## Contact us
For all season of code inquiries, please write us at: gsod@computationalgenomics.ca


## Mentors
[David Bujold](mailto:david.bujold@computationalgenomics.ca)
